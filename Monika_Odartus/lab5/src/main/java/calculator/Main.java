package calculator;

import java.util.Scanner;

public class Main {

    public static Calculator calculator;
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        processCalculation();
    }

    public static void processCalculation () {
        try {
            while (true) {
                System.out.println("Put first number: ");
                Double num1 = scanner.nextDouble();
                System.out.println("Put an operator: ");
                String op = scanner.next();
                System.out.println("Put second number: ");
                Double num2 = scanner.nextDouble();
                calculator = new Calculator(num1, num2);
                Double result = calculator.calculate(op);
                System.out.println("Result = " + result);
                System.out.println("Do you want to continue? [y/n]");
                String ifContinue = scanner.next();
                if (ifContinue.toLowerCase().equals("n")) break;
            }
        } catch (ArithmeticException ae) {
            System.out.println("Dividing by zero is not allowed");
        } catch (Exception e) {
            System.out.println("Operator or number doesn't exist");
        }
    }
}
