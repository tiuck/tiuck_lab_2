package calculator;

public class Calculator {

    private Double firstNumber;
    private Double secondNumber;

    public Calculator (Double firstNumber, Double secondNumber) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }

    public Double getFirstNumber () {
        return this.firstNumber;
    }

    public Double getSecondNumber () {
        return this.secondNumber;
    }

    public Double calculate (String operator) throws Exception {
        if("+".equals(operator))
            return add();
        if("-".equals(operator))
            return subtract();
        if("*".equals(operator))
            return multiply();
        if("/".equals(operator))
            return divide();
        throw new Exception();
    }

    public Double add () {
        return firstNumber + secondNumber;
    }

    public Double subtract () {
        return firstNumber - secondNumber;
    }

    public Double multiply () {
        return firstNumber * secondNumber;
    }

    public Double divide () throws ArithmeticException {
        if(secondNumber == 0){
            throw new ArithmeticException();
        }
        return firstNumber / secondNumber;
    }
}
