import calculator.Calculator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void testAdd() {
        Calculator calculator = new Calculator(1.0,5.0);
        Double result = calculator.add();
        assertEquals((Double) 6.0, result);
    }
    @Test
    public void testSubtract() {
        Calculator calculator = new Calculator(5.0,1.0);
        Double result = calculator.subtract();
        assertEquals((Double) 4.0, result);
    }
    @Test
    public void testMultiply() {
        Calculator calculator = new Calculator(5.0,1.0);
        Double result = calculator.multiply();
        assertEquals((Double) 5.0, result);
    }
    @Test
    public void testDivide() {
        Calculator calculator = new Calculator(5.0,1.0);
        Double result = calculator.divide();
        assertEquals((Double) 5.0, result);
    }
    @Test
    public void testCalculateAdd() throws Exception {
        Calculator calculator = new Calculator(5.0,1.0);
        Double result = calculator.calculate("+");
        assertEquals((Double) 6.0, result);
    }
    @Test
    public void testCalculateSubtract() throws Exception {
        Calculator calculator = new Calculator(5.0,1.0);
        Double result = calculator.calculate("-");
        assertEquals((Double) 4.0, result);
    }
    @Test
    public void testCalculateMultiply() throws Exception {
        Calculator calculator = new Calculator(5.0,1.0);
        Double result = calculator.calculate("*");
        assertEquals((Double) 5.0, result);
    }
    @Test
    public void testCalculateDivide() throws Exception {
        Calculator calculator = new Calculator(5.0,1.0);
        Double result = calculator.calculate("/");
        assertEquals((Double) 5.0, result);
    }
    @Test
    public void testGetFirstNumber() {
        Calculator calculator = new Calculator(5.0,1.0);
        Double result = calculator.getFirstNumber();
        assertEquals((Double) 5.0, result);
    }
    @Test
    public void testGetSecondNumber() {
        Calculator calculator = new Calculator(5.0,1.0);
        Double result = calculator.getSecondNumber();
        assertEquals((Double) 1.0, result);
    }
    @Test
    public void testDivideWithException() throws ArithmeticException {
        exceptionRule.expect(ArithmeticException.class);
        Calculator calculator = new Calculator(5.0,0.0);
        calculator.divide();
    }
    @Test
    public void testCalculateWithException() throws Exception {
        exceptionRule.expect(Exception.class);
        Calculator calculator = new Calculator(5.0,1.0);
        calculator.calculate("^");
    }
}
