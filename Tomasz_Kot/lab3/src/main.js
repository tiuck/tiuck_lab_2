let cubeRotation = 0.0;

startAnimation();


function startAnimation() {
  const canvas = document.querySelector('#glcanvas');
  const gl = canvas.getContext('webgl');

  // if the context is unknown throw error
  if (!gl) {
    alert('Unable to initialize WebGL. Your browser or machine may not support it.');
    return;
  }

  // Initialize the shader program  
  const shader = new Shader();
  const shaderProgram = shader.initShaderProgram(gl, vsSource, fsSource);

  // Initialize the object needed
  const storage = new Storage()
  const texture_c = new Texture()
  const myCanvas = new Canvas()
  
  const storages = storage.initStorages(gl);
  const texture = texture_c.loadTexture(gl, 'cubetexture.png');

 // collect the information needed by the program
  const programInfo = {
    program: shaderProgram,
    attribLocations: {
      vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
      vertexNormal: gl.getAttribLocation(shaderProgram, 'aVertexNormal'),
      textureCoord: gl.getAttribLocation(shaderProgram, 'aTextureCoord'),
    },
    uniformLocations: {
      projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
      modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
      normalMatrix: gl.getUniformLocation(shaderProgram, 'uNormalMatrix'),
      uSampler: gl.getUniformLocation(shaderProgram, 'uSampler'),
    },
  };

  var then = 0;

// Draw the canvas in the loop
function render(now) {
    now *= 0.001;  // convert to seconds
    const deltaTime = now - then;
    then = now;
    myCanvas.drawCanvas(gl, programInfo, storages, texture, deltaTime);
    requestAnimationFrame(render);
  }
  requestAnimationFrame(render);
}

function isPowerOf2(value) {
  return (value & (value - 1)) == 0;
}
