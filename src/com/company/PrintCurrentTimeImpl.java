package com.company;

import com.company.PrintCurrentTime;
import java.util.Date;

public class PrintCurrentTimeImpl implements PrintCurrentTime {

    @Override
    public String print() {
        Date timeNow = new Date();
        String timeNowStr = timeNow.toString();
        return timeNowStr;
    }
}
