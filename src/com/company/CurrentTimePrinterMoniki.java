package com.company;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CurrentTimePrinterMoniki implements PrintCurrentTime{

    public String print(){
        Date time = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        return formatter.format(time);
    };
}
