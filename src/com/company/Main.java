package com.company;

import com.company.mateusz.time.CurrentTime;

public class Main {

    public static void main(String[] args) {
        int option = Integer.parseInt(args[0]);
        switch(option) {
            case 1:
                CurrentTimePrinterMoniki crm = new CurrentTimePrinterMoniki();
                System.out.println(crm.print());
                break;
            case 2:
                PrintCurrentTimeImpl impl = new PrintCurrentTimeImpl();
                System.out.println(impl.print());
                break;
            case 3:
                MyClock myClock = new MyClock();
                System.out.println(myClock.print());
                break;
            case 4:
                CurrentTime currentTime = new CurrentTime();
                System.out.println(currentTime.print());
                break;
            default:
                System.out.println("No such option");
        }
    }
}
