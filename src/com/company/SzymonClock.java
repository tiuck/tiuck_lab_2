package com.company;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

class MyClock implements PrintCurrentTime {
    public String print() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return dateTimeFormatter.format(now);
    }
}