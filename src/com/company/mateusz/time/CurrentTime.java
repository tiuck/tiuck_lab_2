package com.company.mateusz.time;

import com.company.PrintCurrentTime;

import java.time.LocalDateTime;

public class CurrentTime implements PrintCurrentTime {
    @Override
    public String print() {
        LocalDateTime currentTime = LocalDateTime.now();
        return currentTime.toString();
    }
}
