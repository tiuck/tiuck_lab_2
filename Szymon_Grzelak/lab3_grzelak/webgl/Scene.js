class Scene{

 drawScene(gl, programInfo, buffers, texture, deltaTime) {
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clearDepth(1.0);              
    gl.enable(gl.DEPTH_TEST);           
    gl.depthFunc(gl.LEQUAL);           
  
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  
    const fieldOfView = 45 * Math.PI / 180; 
    const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
    const zNear = 0.1;
    const zFar = 100.0;
    const projectionMatrix = mat4.create();
  
    mat4.perspective(projectionMatrix,
                     fieldOfView,
                     aspect,
                     zNear,
                     zFar);

    const modelViewMatrix = mat4.create();
  
    mat4.translate(modelViewMatrix,     
                   modelViewMatrix,     
                   [-0.0, 0.0, -6.0]);  
    mat4.rotate(modelViewMatrix,  
                modelViewMatrix,  
                cubeRotation,     
                [0, 0, 1]);       
    mat4.rotate(modelViewMatrix,  
                modelViewMatrix, 
                cubeRotation * .7,
                [0, 1, 0]);       
  
    const normalMatrix = mat4.create();
    mat4.invert(normalMatrix, modelViewMatrix);
    mat4.transpose(normalMatrix, normalMatrix);
  
    this.pullOutFromBuffer(gl, buffers.position, 3, 
        programInfo.attribLocations.vertexPosition);
    this.pullOutFromBuffer(gl, buffers.textureCoord, 2, 
        programInfo.attribLocations.textureCoord);
    this.pullOutFromBuffer(gl, buffers.normal, 3, 
        programInfo.attribLocations.vertexNormal);
    
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices);
    gl.useProgram(programInfo.program);

    gl.uniformMatrix4fv(
        programInfo.uniformLocations.projectionMatrix,
        false,
        projectionMatrix);
    gl.uniformMatrix4fv(
        programInfo.uniformLocations.modelViewMatrix,
        false,
        modelViewMatrix);
    gl.uniformMatrix4fv(
        programInfo.uniformLocations.normalMatrix,
        false,
        normalMatrix);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.uniform1i(programInfo.uniformLocations.uSampler, 0);
  
    {
      const vertexCount = 36;
      const type = gl.UNSIGNED_SHORT;
      const offset = 0;
      gl.drawElements(gl.TRIANGLES, vertexCount, type, offset);
    }
  
    cubeRotation += deltaTime;
  }
  

    pullOutFromBuffer(gl, buffer, numComponents, attribLocation) {
        {
            const type = gl.FLOAT;
            const normalize = false;
            const stride = 0;
            const offset = 0;
            gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
            gl.vertexAttribPointer(attribLocation, 
                numComponents, type, normalize, stride, offset);
            gl.enableVertexAttribArray(attribLocation);
        }
    }
}