import static junit.framework.TestCase.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

public class TestCalculator {
  @Rule public MockitoRule rule = MockitoJUnit.rule();

  @Rule public ExpectedException exceptionRule = ExpectedException.none();

  Calculator calculator1;

  @Before
  public void init() {
    calculator1 = new Calculator(0.0);
    calculator1.setFirstComponent(1.0);
  }

  @Test
  public void add() {
    // when(calculator.add(1.5)).thenReturn(1.3);
    assertEquals(2.3, calculator1.add(1.3));
  }

  @Test
  public void multiple() {
    assertEquals(5.0, calculator1.multiple(5.0));
  }

  @Test
  public void minus() {
    assertEquals(5.0, calculator1.minus(-4.0));
  }

  @Test
  public void divide() {
    try {
      assertEquals(5.0, calculator1.divide(0.2));
    } catch (DivideByZero divideByZero) {
      divideByZero.printStackTrace();
    }
  }

  @Test
  public void divide_by_zero() throws DivideByZero {
    exceptionRule.expect(DivideByZero.class);
    exceptionRule.expectMessage("You cannot divide by zero\n");
    calculator1.divide(0.0);
  }
}
