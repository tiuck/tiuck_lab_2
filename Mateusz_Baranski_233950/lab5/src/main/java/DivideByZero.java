public class DivideByZero extends Exception {
  public DivideByZero() {
    super("You cannot divide by zero\n");
  }
}
