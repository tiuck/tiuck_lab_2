public class Calculator {
  private Double firstComponent;

  public Double getFirstComponent() {
    return firstComponent;
  }

  public void setFirstComponent(Double firstComponent) {
    this.firstComponent = firstComponent;
  }

  public Calculator(Double firstComponent) {
    this.firstComponent = firstComponent;
  }

  public Double add(Double a) {
    return firstComponent + a;
  }

  public Double minus(Double a) {
    return firstComponent - a;
  }

  public Double multiple(Double a) {
    return firstComponent * a;
  }

  public Double divide(Double a) throws DivideByZero {
    if (a.equals(0.0)) throw new DivideByZero();
    return firstComponent / a;
  }
}
